package com.example.miniexcercise1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.os.Bundle;


public class Activity4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_three);
    }
    public void goToActivityMain(View view){
        startActivity(new Intent(Activity4.this, MainActivity.class));
    }
}
