package com.example.miniexcercise1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Activity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_two);
    }
    public void goToActivityMain(View view){
        startActivity(new Intent(Activity3.this, MainActivity.class));
    }
}
